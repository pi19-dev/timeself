package lstu.pi19.timeself.data;

//Используется паттерн одиночка
//т.к. есть только один user
//для создания экземпляра используйте getUser()

public class User {

    private static User user;

    private String password;
    private String login;
    private String firstName;
    private String lastName;
    private String id;
    private String accessToken; // TODO Не давай его никому больше)00)

    public int n;

    private User() { }

    synchronized public static User getUser(){
        if(User.user == null){
            User.user = new User();
        }
        return User.user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}