package lstu.pi19.timeself.data.calendar;

import android.widget.LinearLayout;

import java.util.Calendar;

public class Card {
    private int color;    //цвет которым мы кодируем карточку
    private boolean completed;

    public int getColor() {
        return color;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setCardTime(CardTime cardTime) {
        this.cardTime = cardTime;
    }

    public CardTime getCardTime() {
        return cardTime;
    }

    private String title, description;
    //необязательно
    private String address;

    private CardTime cardTime;

    public Card(String title, String description, int color) {
        this.color = color;
        this.title = title;
        this.description = description;
    }


    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed= completed;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public class CardTime{
        public Calendar getStartTime() {
            return startTime;
        }

        public Calendar getEndTime() {
            return endTime;
        }

        //Внутрений класс чтобы не нагромждать конструктор
        java.util.Calendar startTime , endTime;

        public CardTime(java.util.Calendar startTime) {
            this.startTime = startTime;
        }

        public CardTime(java.util.Calendar startTime, java.util.Calendar endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

    }
}
