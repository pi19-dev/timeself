package lstu.pi19.timeself.data.group;

import java.util.List;

import lstu.pi19.timeself.data.User;
import lstu.pi19.timeself.data.calendar.Card;

public class Group {
    private String name;
    //List<groupUser> groupUsers;
    List<Card> cards;

    public String getName() {
        return name;
    }

    public Group(String name){
        this.name = name;
        this.isPrivate = false;
    }

    public Group(String name , boolean isPrivate){
        this.name = name;
        this.isPrivate = isPrivate;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    boolean isPrivate;
    boolean idDeleted;
}
