package lstu.pi19.timeself.data.calendar;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CardList {

    private static ArrayList<Card> cards;
    static CardList cardList;

    private CardList() {

    }

    public static CardList getCardsList() {
        if(cards == null) {
            cardList = new CardList();
            cards =new ArrayList<>();
            initializeData();
        }
        return cardList;
    }

    public ArrayList<Card> getCards(){
        return cards;
    }

    private static void initializeData() {
        java.util.Calendar calendar = new GregorianCalendar(2021 , 4 , 26);
        calendar.set(java.util.Calendar.HOUR, 10);


        java.util.Calendar calendar1 = new GregorianCalendar(2021 , 4 , 25);
        calendar1.set(java.util.Calendar.HOUR, 11);

        java.util.Calendar calendar2 = new GregorianCalendar(2021 , 4 , 27);
        calendar2.set(Calendar.HOUR, 12);


        Card card = new Card("Сделать первую карточку на этом проекте", "Спустя n-ое колличесво времени на проект я сделал карточку"  , Color.BLUE);
        Card.CardTime cardTime = card.new CardTime(calendar, calendar1);
        card.setCardTime(cardTime);
        cards.add(card);

        Card card1 = new Card("Тигрить", "Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр Мур мур мяу тигр мур мяр мур тигр мур мур мяу тигр"  , Color.RED);
        Card.CardTime cardTime1 = card1.new CardTime(calendar1 , calendar1);
        card1.setCardTime(cardTime1);
        cards.add(card1);

        Card card3 = new Card(null, "Карточка может быть без имени"  , Color.RED);
        Card.CardTime cardTime3 = card3.new CardTime(calendar1 );
        card3.setCardTime(cardTime3);
        cards.add(card3);


        Card card2 = new Card("Без времени" , "Карточка может быть без времени" , android.R.color.holo_green_light);
        cards.add(card2);
    }
}
