package lstu.pi19.timeself.data.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Calendar;

//Наша бд
//SQLite
//Трогать на свой страх и риск
public class CardBD extends SQLiteOpenHelper {
    private static final String DB_NAME = "card";
    private static final int DB_VERSION = 1;


    public CardBD(Context context){
        super(context , DB_NAME , null ,DB_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatebase(db , 0  , DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatebase(db , oldVersion , newVersion);
    }

    public void insertCard(SQLiteDatabase db , String title , String description , Calendar startTime , Calendar endTime , Calendar repeatTime, boolean completed){
        ContentValues cardValues = new ContentValues();
        cardValues.put("TITLE" , title);
        cardValues.put("DESCRIPTION", description);
        cardValues.put("START_TIME" , startTime.getTime().getTime());
        cardValues.put("END_TIME", endTime.getTime().getTime());
        cardValues.put("REPEAT_TIME", repeatTime.getTime().getTime());
        cardValues.put("COMPLETED" , completed);
        db.insertOrThrow("CARD" , null , cardValues);
    }

    private void updateMyDatebase(SQLiteDatabase db , int oldVersion , int newVersion){
        if(oldVersion<1){
            db.execSQL("CREATE TABLE CARD ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "TITLE TEXT, "
            + "DESCRIPTION TEXT, "
                    + "START_TIME INTEGER, "
                    + "END_TIME INTEGER, "
                    + "REPEAT_TIME INTEGER, "
                    + "COMPLETED INTEGER) ;");   //Не нашел в SQL тип boolean
        }
    }

}
