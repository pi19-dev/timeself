package lstu.pi19.timeself.api.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class LightGroupResponse {
    String name;
    int subscribersCount;
}
