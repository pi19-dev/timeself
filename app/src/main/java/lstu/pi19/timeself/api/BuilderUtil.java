package lstu.pi19.timeself.api;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class BuilderUtil {

    static final OkHttpClient client = new OkHttpClient();

    public static Request.Builder withAuth(String token){

        return new Request.Builder().
                header("Authorization" , "Bearer " + token);
    }

    public static HttpUrl.Builder urlBuilder(){
        return new HttpUrl.Builder().host("localhost").port(8080);
    }

    public static OkHttpClient getClient() {
        return client;
    }


}
