package lstu.pi19.timeself.api;

import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

import lstu.pi19.timeself.api.responses.GroupResponse;
import lstu.pi19.timeself.api.responses.LightGroupResponse;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class GroupApi extends GeneralAPIConfig {

    @Override
    HttpUrl.Builder innerUrl() {
        return clientUtil.originUrl().addPathSegment("group");
    }

    public String createGroup(String userId, GroupReqBody body) throws IOException {
        FormBody.Builder requestBody = new FormBody.Builder()
                .add("description", body.getDescription())
                .add("name", body.getName())
                .add("isPrivate", body.getIsPrivate().toString());

        Request request = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment("new")
                        .addPathSegment(userId)
                        .build())
                .put(requestBody.build())
                .build();

        try (final Response response = client.newCall(request).execute()) {
            switch (response.code()) {
                case 200:
                    String groupId = response.body().string();
                    return groupId;
                case 500:
                default:
                    break;
            }
        }
        return null;
    }

    public GroupResponse getGroup(String groupId) throws IOException {
        Request request = clientUtil
                .authRequestBuilder()
                .url(innerUrl()
                        .addPathSegment(groupId)
                        .build())
                .build();

        final GroupResponse group;
        try (final Response response = client.newCall(request).execute()) {
            switch (response.code()) {
                case 200:
                    group = objectMapper.readValue(response.body().string(), GroupResponse.class);
                    break;
                case 404:
                case 500:
                default:
                    group = null;
                    break;
            }
        }
        return group;
    }

    public List<LightGroupResponse> getGroups(String page, String count) throws IOException {
        Request request = clientUtil
                .authRequestBuilder()
                .url(innerUrl()
                        .addQueryParameter("page", page)
                        .addQueryParameter("count", count)
                        .build())
                .build();

        final List<LightGroupResponse> group;
        try (final Response response = client.newCall(request).execute()) {
            switch (response.code()) {
                case 200:
                    group = objectMapper.readValue(
                                response.body().string(),
                                new TypeReference<List<LightGroupResponse>>(){}
                            );
                    break;
                case 404:
                case 500:
                default:
                    group = null;
                    break;
            }
        }
        return group;
    }

    public boolean updateGroup(String groupId, GroupReqBody body) throws IOException {
        FormBody.Builder requestBody = new FormBody.Builder();
        if (body.getDescription() != null)
            requestBody.add("description", body.getDescription());
        if (body.getName() != null)
            requestBody.add("name", body.getName());
        if (body.getIsPrivate() != null)
            requestBody.add("isPrivate", body.getIsPrivate().toString());

        Request request = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment(groupId)
                        .build())
                .post(requestBody.build())
                .build();

        try (final Response response = client.newCall(request).execute()) {
            switch (response.code()) {
                case 200:
                    boolean isSucceeded = Boolean.parseBoolean(response.body().string());
                    return isSucceeded;
                case 404:
                case 500:
                default:
                    // TODO ФРОНТЫ, ОТ ВАС ЭКСЕПШЕНЫ НА 404, 500 УДОБНЫМ ВАМ СПОСОБОМ
                    break;
            }
        }
        return false;
    }
}
