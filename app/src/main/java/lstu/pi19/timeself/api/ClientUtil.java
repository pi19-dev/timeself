package lstu.pi19.timeself.api;

import okhttp3.HttpUrl;
import okhttp3.Request;

public class ClientUtil { // Singleton

    private ClientUtil() {}

    private static ClientUtil clientUtilInstance;

    public static ClientUtil clientUtil() {
        if (clientUtilInstance == null) {
            clientUtilInstance = new ClientUtil();
        }
        return clientUtilInstance;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String newToken) {
        token = newToken;
    }

    public Request.Builder authRequestBuilder() {
        return new Request.Builder()
                .addHeader("Authorization", "Bearer " + token);
    }

    public HttpUrl.Builder originUrl() {
        return new HttpUrl.Builder()
                .host("localhost") // Только если запускаешь эмулятор на одном компе с сервером
                //.host("") // Не знаю пока как адрес сервера указать
                .port(8080);
    }
}
