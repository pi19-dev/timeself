package lstu.pi19.timeself.api.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class GroupResponse {
    String name;
    int subscribersCount;
    int tasksCount;
    String description;
}
