package lstu.pi19.timeself.api;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import lstu.pi19.timeself.data.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class AuthApi extends GeneralAPIConfig {

    private final ClientUtil clientUtil = ClientUtil.clientUtil();

    @Override
    public HttpUrl.Builder innerUrl() {
        return clientUtil.originUrl()
                .addPathSegment("auth"); // TODO Может нужен "/"
    }

    public void register(User user) {
        RequestBody registerRequestBody = new FormBody.Builder()
                .add("email", user.getLogin())
                .add("password", user.getPassword())
                .add("firstName", user.getFirstName())
                .add("lastName", user.getLastName())
                .build();

        Request registerRequest = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment("register") // TODO Может нужен "/"
                        .build())
                .put(registerRequestBody)
                .build();

        // Асинхронный вызов, взял с https://square.github.io/okhttp/recipes/#asynchronous-get-kt-java
        // Не бойся, он генерится сам первый раз, а дальше копируй, но с умом
        client.newCall(registerRequest).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody body = response.body()) {
                    switch (response.code()) {
                        case 200: // Все по кайфу (наверн)
                            user.setId(body.string());
                            break;
                        case 400: // TODO Пользователь уже существует. Сделай вывод на экран
                            break;
                        case 500: // Внутренняя ошибка сервера. На бэке надо что-то исправить D:
                                // TODO Везде, где такая ошибка, делай вывод: "Что-то пошло не так на сервере"
                            break;
                    }
                }
            }
        });
    }

    public void login(User user) {
        RequestBody loginRequestBody = new FormBody.Builder()
                .add("email", user.getLogin())
                .add("password", user.getPassword())
                .build();

        Request loginRequest = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment("login")  // TODO Может нужен "/"
                        .build())
                .post(loginRequestBody)
                .build();

        client.newCall(loginRequest).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody body = response.body()) {
                    switch (response.code()) {
                        case 200:
                            user.setAccessToken(body.string());
                            break;
                        case 500: // TODO
                            break;
                    }
                }
            }
        });
    }

    public void logout(User user) {
        RequestBody requestBody = new FormBody.Builder()
                .add("token", user.getAccessToken())
                .build();

        Request request = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment("logout")  // TODO Может нужен "/"
                        .build())
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody body = response.body()) {
                    switch (response.code()) {
                        case 200:
                            // todo вывод на экран: Logout success
                            break;
                        case 500: // TODO
                            break;
                    }
                }
            }
        });
    }

    // Этот метод может и не понадобится никогда, без понятия
    public void validateToken(User user) {
        RequestBody requestBody = new FormBody.Builder()
                .add("token", user.getAccessToken())
                .build();

        Request request = new Request.Builder()
                .url(innerUrl()
                        .addPathSegment("validateToken")  // TODO Может нужен "/"
                        .build())
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody body = response.body()) {
                    switch (response.code()) {
                        case 200:
                            break;
                        case 400:
                            break;
                        case 500:
                            break;
                    }
                }
            }
        });
    }
}
