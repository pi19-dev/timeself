package lstu.pi19.timeself.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

public abstract class GeneralAPIConfig {

    protected final OkHttpClient client = new OkHttpClient();
    protected final ClientUtil clientUtil = ClientUtil.clientUtil();
    protected final ObjectMapper objectMapper = new ObjectMapper();

    abstract HttpUrl.Builder innerUrl();
}
