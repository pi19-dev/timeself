package lstu.pi19.timeself.api;

import lombok.Data;

@Data
public class GroupReqBody {
    String name;
    Boolean isPrivate;
    String description;
}
