package lstu.pi19.timeself;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.User;
import lstu.pi19.timeself.ui.NavigationController;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity implements NavigationController {

    NavController navController;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        user = User.getUser();

    }

    @Override
    public void goToLogin() {
        navController.popBackStack();
        navController.navigate(R.id.navigation_login);
    }

    @Override
    public void goToRegistration() {
        navController.navigate(R.id.action_navigation_login_to_navigation_registration);
    }

    @Override
    public void goToFragmentEditing(int i) {
        navController.navigate(R.id.action_navigation_calendar_to_navigation_cardEditing);
        idEditing = i;
    }

    private static int idEditing , idGroup;
    @Override
    public int goEditingId(){
        return idEditing;
    }
    @Override
    public int goGroupId(){
        return idGroup;
    }

    @Override
    public void goToGroupCreate() {
        navController.navigate(R.id.navigation_group_create);
    }

    @Override
    public void goToGroupDescription(int id) {
        navController.navigate(R.id.navigation_group_profile);
        idGroup =id;
    }

    @Override
    public void popBackStack(){
        navController.popBackStack();
    }

}