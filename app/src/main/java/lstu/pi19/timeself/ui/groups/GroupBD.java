package lstu.pi19.timeself.ui.groups;

import java.util.ArrayList;

import lstu.pi19.timeself.data.group.Group;

public class GroupBD {

    private static ArrayList<Group> groups;
    static GroupBD groupList;

    private GroupBD() {

    }

    public static GroupBD getGroupList() {
        if(groups == null) {
            groupList = new GroupBD();
            groups =new ArrayList<>();
            initializeData();
        }
        return groupList;
    }

    public ArrayList<Group> getGroups(){
        return groups;
    }

    private static void initializeData() {
        groups.add(new Group("Лгту"));
        groups.add(new Group("Сейфнет"));
        groups.add(new Group("ПИ 19-1"));
        groups.add(new Group("Тигры"));
    }
}
