package lstu.pi19.timeself.ui.groups;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.group.Group;
import lstu.pi19.timeself.ui.NavigationController;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.PersonGroupHolder>  {


    public class PersonGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        private TextView title , description;
        private CardView cardView;
        int id;

        public PersonGroupHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_group);
            //description = itemView.findViewById(R.id.desctiption_group);
            cardView = itemView.findViewById(R.id.cardView);

            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onMyClick(id);
        }
    }

    String TAG = "AdapterGroup";
    List<Group> groupList;
    Context context;

    GroupAdapter(ArrayList<Group> groupList  , Context context){
        this.groupList = groupList;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public PersonGroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_card, parent, false);
        PersonGroupHolder pgh = new PersonGroupHolder(v);
        return pgh;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonGroupHolder holder, int i) {
        holder.id = i;

        Log.i(TAG , "onBindViewHolder");
        holder.title.setText(groupList.get(i).getName());
    }


    public void onMyClick(int id){
        NavigationController navControler = (NavigationController) context;
        navControler.goToGroupDescription(id);
    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }


}
