package lstu.pi19.timeself.ui.calendar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.calendar.Card;
import lstu.pi19.timeself.data.calendar.CardList;

public class СalendarFragment extends Fragment {

    RecyclerView recyclerView;
    boolean isEditing = false;

    static boolean date = true;
    CalendarView calendarView;
    public static final String TAG = "CalendarFragment";
    private ArrayList<Card> cardList = new ArrayList<Card>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        recyclerView = view.findViewById(R.id.resylerView);
        calendarView = view.findViewById(R.id.calendarView);

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        initializeAdapter();

        Calendar calendar = Calendar.getInstance();
        getCardsCalendar(calendar.get(Calendar.YEAR) , calendar.get(Calendar.MONTH) , calendar.get(Calendar.DAY_OF_MONTH));


        getCalendarDate(calendarView);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initializeAdapter() {
        CardTextAdapter adapterText = new CardTextAdapter(cardList, getContext());
        recyclerView.setAdapter(adapterText);
    }


    private void getCalendarDate(CalendarView calendarView) {
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                int mYear = year;
                int mMonth = month;
                int mDay = dayOfMonth;
                getCardsCalendar(year , month , dayOfMonth);
            }
        });
    }

    private void getCardsCalendar(int year, int month, int dayOfMonth){
        cardList.clear();
        for (Card card : CardList.getCardsList().getCards()) {
            if (card.getCardTime() ==null ||card.getCardTime().getStartTime() ==null ||
                    (card.getCardTime().getStartTime().get(Calendar.YEAR) == year) &&
                            (card.getCardTime().getStartTime().get(Calendar.MONTH) == month) &&
                            (card.getCardTime().getStartTime().get(Calendar.DAY_OF_MONTH) == dayOfMonth)) {
                cardList.add(card);
            }
            initializeAdapter();

            Log.i(TAG, "getCalendarDate " + year + " " + month + " " + dayOfMonth);
        }
    }
}
