package lstu.pi19.timeself.ui;

public interface NavigationController {
    void goToLogin();
    void goToRegistration();
    void goToFragmentEditing(int n);
    int goEditingId();
    int goGroupId();
    void goToGroupCreate();
    void goToGroupDescription(int n);
    void popBackStack();
}
