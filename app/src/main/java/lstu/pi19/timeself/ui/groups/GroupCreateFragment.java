package lstu.pi19.timeself.ui.groups;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.ArrayList;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.group.Group;
import lstu.pi19.timeself.ui.NavigationController;

public class GroupCreateFragment extends Fragment implements View.OnClickListener {

    EditText groupNameEditText;
    CheckBox privateGroupCheckBox;
    Button createGroupButton, cancelCreateGroupButton;
    private String TAG = "GroupCreateFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_create, container, false);
        groupNameEditText = (EditText) view.findViewById(R.id.edit_name_group);
        privateGroupCheckBox = (CheckBox) view.findViewById(R.id.private_group_checkBox);
        createGroupButton = (Button) view.findViewById(R.id.group_create_button);
        cancelCreateGroupButton = (Button) view.findViewById(R.id.group_cancel_button);

        createGroupButton.setOnClickListener(this::onClick);
        cancelCreateGroupButton.setOnClickListener(this::onClick);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.group_create_button:
                addGroup();
                break;
            case R.id.group_cancel_button:
                cancel();
                break;
        }
    }

    private void addGroup(){
        Log.i(TAG , "addGroup()");
        GroupBD groupBD = GroupBD.getGroupList();
        ArrayList<Group> groups = groupBD.getGroups();

        String name = groupNameEditText.getText().toString();
        boolean privateGroup = privateGroupCheckBox.isChecked();

        Group group = new Group(name, privateGroup);

        groups.add(group);
        NavigationController navigationController = (NavigationController) getActivity();
        navigationController.popBackStack();
    }

    private void cancel(){
        NavigationController navigationController = (NavigationController) getActivity();
        navigationController.popBackStack();
    }
}