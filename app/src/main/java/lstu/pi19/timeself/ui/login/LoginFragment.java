package lstu.pi19.timeself.ui.login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.ui.NavigationController;

public class LoginFragment extends Fragment implements View.OnClickListener {

    Button loginButton, registrationButton;
    EditText loginEdit, passwordEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        loginButton = (Button) view.findViewById(R.id.button_login);
        registrationButton = (Button) view.findViewById(R.id.button_registration);
        loginEdit = (EditText) view.findViewById(R.id.edit_login);
        passwordEdit = (EditText) view.findViewById(R.id.edit_password);


        loginButton.setOnClickListener(this::onClick);
        registrationButton.setOnClickListener(this::onClick);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                readEdit();
                checkLogin(loginEdit.toString());
                checkPassword(loginButton.toString());
                break;
            case R.id.button_registration:
                NavigationController navCont = (NavigationController) getActivity();
                navCont.goToRegistration();
                break;
        }
    }

    public void readEdit() {
        String login = loginEdit.getText().toString();
        String password = passwordEdit.getText().toString();
    }

    public boolean checkLogin(String login) {
        if (login.length() > 3 && login.indexOf('@') > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkPassword(String login) {
        if (login.length() > 3) {
            return true;
        } else {
            return false;
        }
    }

}