package lstu.pi19.timeself.ui.calendar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.GregorianCalendar;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.calendar.Card;
import lstu.pi19.timeself.data.calendar.CardList;
import lstu.pi19.timeself.ui.NavigationController;

public class EditingCard extends Fragment {

    int idCard ;
    public static String TAG = "EditingCard";
    Card card;

    EditText title , description , startTime, endTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_editing_card, container, false);

        title = view.findViewById(R.id.title);
        description = view.findViewById(R.id.desctiption);
        startTime = view.findViewById(R.id.startTime);
        endTime = view.findViewById(R.id.endTime);

        NavigationController navigationController = (NavigationController) getContext();
        idCard = navigationController.goEditingId();
        Log.i(TAG , "id card = " + idCard);

        CardList cardList=CardList.getCardsList();
        card= cardList.getCards().get(idCard);
        filling();

        return view;
    }

    private void filling(){
        Log.i(TAG , "inf");
        title.setText( card.getTitle());
        description.setText( card.getDescription());
        if(card.getCardTime() != null) {
            fillingStartTime();
            fillingEndTime();
        }
    }

    private void fillingStartTime(){
        if(card.getCardTime().getStartTime()!= null){
            Calendar calendar =  card.getCardTime().getStartTime();
            startTime.setText(getTimeFormat(calendar));
        }
    }

    private void fillingEndTime(){
        if(card.getCardTime().getEndTime()!= null){
            Calendar calendar =  card.getCardTime().getEndTime();
            endTime.setText(getTimeFormat(calendar));
        }
    }

    public String getTimeFormat(Calendar time){
        String minute =String.valueOf(time.get(Calendar.MINUTE));
        if(minute.length()<2) minute = '0' + minute;

        return time.get(Calendar.HOUR_OF_DAY) + ":" + minute  ;
    }

    private void save(){
        Log.i(TAG , "Save");
        Log.i(TAG , title.getText().toString());

        card.setTitle(title.getText().toString());
        card.setDescription(description.getText().toString());
        textViewStartTime(startTime.getText().toString());
        textViewEndTime(endTime.getText().toString());
    }

    private void textViewStartTime(String startTime){
        try {
            int c = startTime.indexOf(':');
            int hour = Integer.parseInt(startTime.substring(0, c));
            int minute = Integer.parseInt(startTime.substring(c+1, startTime.length()));

            if(card.getCardTime().getStartTime() != null) {
                card.getCardTime().getStartTime().set(Calendar.HOUR_OF_DAY, hour);
                card.getCardTime().getStartTime().set(Calendar.MINUTE , minute);
            }else {
                Calendar calendar = new GregorianCalendar(0 , 0 , 0);
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                card.setCardTime(card.new CardTime(calendar));
            }

        }catch (Exception e){
            Log.e(TAG , "Не верно указано время startTime");
        }
    }

    private void textViewEndTime(String endTime){
        try {
            int c = endTime.indexOf(':');
            int hour = Integer.parseInt(endTime.substring(0, c));
            int minute = Integer.parseInt(endTime.substring(c+1, startTime.length()));

            card.getCardTime().getEndTime().set(Calendar.HOUR_OF_DAY , hour);
            card.getCardTime().getEndTime().set(Calendar.MINUTE , minute);
        }catch (Exception e){
            Log.e(TAG , "Не верно указано время endTime" );
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        save();
    }
}