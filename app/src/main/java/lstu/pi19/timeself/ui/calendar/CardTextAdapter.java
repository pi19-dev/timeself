package lstu.pi19.timeself.ui.calendar;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.calendar.Card;
import lstu.pi19.timeself.ui.NavigationController;

public class CardTextAdapter extends RecyclerView.Adapter<CardTextAdapter.PersonViewHolder> {

    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cv , cardView;
        TextView title;
        TextView descripton;
        TextView startTime , endTime;
        ImageButton imageButton;
        int id;


        CheckBox completedBox;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            imageButton = itemView.findViewById(R.id.imageView);
            cv = itemView.findViewById(R.id.resylerView);
            cardView = itemView.findViewById(R.id.eventsCalenarCard);
            title = itemView.findViewById(R.id.title);
            descripton = itemView.findViewById(R.id.desctiption);
            startTime = itemView.findViewById(R.id.startTime);
            endTime = itemView.findViewById(R.id.endTime);
            completedBox = itemView.findViewById(R.id.completed);

            imageButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imageView:
                    pencilClick(id);
            }
        }
    }

    public void pencilClick(int i){
        NavigationController navigationController = (NavigationController) context;
        navigationController.goToFragmentEditing(i);
    }
    List<Card> cards;
    Context context;
    public final String TAG = "PVAdapter";

    CardTextAdapter(ArrayList<Card> cards , Context context){
        this.cards =cards;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent , false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }


    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int i) {
        //возможно стоит использывать try catch
        //holder.cardView.setBackgroundColor(cards.get(i).getColor());
        holder.id = i;

        if(cards.get(i).getTitle() != null) {
            holder.title.setText(cards.get(i).getTitle());
        }else{
            holder.title.setHeight(0);
        }

        holder.descripton.setText(cards.get(i).getDescription());

        if(cards.get(i).getCardTime()!=null) {
            textViewStartTime(holder, i);
            textViewEndTime(holder, i);
        }else {
            //можно убрать в конце разработки, используется для наглядности макета
            holder.startTime.setText("");
            holder.endTime.setText("");
        }

        cards.get(i).setCompleted(holder.completedBox.isChecked());
    }


    private void textViewStartTime(@NonNull PersonViewHolder holder, int i){
        if (cards.get(i).getCardTime().getStartTime() != null) {
            holder.startTime.setText(getTimeFormat(cards.get(i).getCardTime().getStartTime()));
        } else {
            holder.startTime.setText("");
        }
    }

    private void textViewEndTime(@NonNull PersonViewHolder holder, int i){
        if (cards.get(i).getCardTime().getEndTime() != null) {
            holder.endTime.setText(getTimeFormat(cards.get(i).getCardTime().getEndTime()));
        } else {
            holder.endTime.setText("");
        }
    }

    public String getTimeFormat(Calendar time){
        String minute =String.valueOf(time.get(Calendar.MINUTE));
        if(minute.length()<2) minute = '0' + minute;

        return time.get(Calendar.HOUR_OF_DAY) + " " + minute  ;
    }

    @Override
    public int getItemCount() {
        Log.d(TAG , "peopleSize = " + cards.size());
        return cards.size();
    }

}

