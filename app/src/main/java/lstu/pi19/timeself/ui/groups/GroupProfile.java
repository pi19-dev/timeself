package lstu.pi19.timeself.ui.groups;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.group.Group;
import lstu.pi19.timeself.ui.NavigationController;

public class GroupProfile extends Fragment {

    //Сделано имя группы и кнопка подписки


    private TextView nameTextV , privateTextV;
    private Group group;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_profile, container, false);
        nameTextV = view.findViewById(R.id.group_name_textView);
        privateTextV = view.findViewById(R.id.info_group_textView);

        initializationView();
        return view;
    }


    void initializationView(){
        NavigationController navigationController = (NavigationController)getActivity();

        GroupBD groupBD = GroupBD.getGroupList();
        ArrayList<Group> groups = groupBD.getGroups();
        group = groups.get(navigationController.goGroupId());

        nameTextV.setText(group.getName());

        if(group.isPrivate()){
            privateTextV.setText("Приватная");
        }else{
            privateTextV.setText("Публичная");
        }
    }

}