package lstu.pi19.timeself.ui.profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.ui.NavigationController;

public class ProfileFragment extends Fragment  implements View.OnClickListener {

    Button isActiveButton;
    boolean isActive = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        isActiveButton = (Button) view.findViewById(R.id.button_isactive);
        isActiveButton.setOnClickListener(this);

        checkActive();
        return view;
    }

    private void isActiveButtonClick() {
        if (isActive) isActive = false;
        else
            isActive = true;
        checkActive();
    }

    private void checkActive() {
        if (isActive) isActiveButton.setText("Выйти");
        else
            isActiveButton.setText("Войти");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_isactive:
                NavigationController navigationController = (NavigationController) getActivity();
                navigationController.goToLogin();
                isActiveButtonClick();
                break;
        }
    }
}