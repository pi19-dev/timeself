package lstu.pi19.timeself.ui.groups;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.ui.NavigationController;

public class GroupListFragment extends Fragment implements View.OnClickListener {
    RecyclerView recyclerView;
    GroupBD groups;

    final static String TAG = "GroupListFragment";
    private Toolbar toolbar;
    private ImageButton createGroupButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i(TAG , "onCreateView");

        View view = inflater.inflate(R.layout.fragment_group_list, container, false);
        recyclerView = view.findViewById(R.id.resylerViewGroup);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar2);
        createGroupButton = (ImageButton) view.findViewById(R.id.group_create_button_toolbar);
        createGroupButton.setOnClickListener(this::onClick);

        AppCompatActivity activity = (AppCompatActivity)getActivity();
        activity.setSupportActionBar(toolbar);

        groups = GroupBD.getGroupList();

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);

        initializationAdapter();

        return view;
    }



    private void initializationAdapter(){
        Log.i(TAG , "initializationAdapter");
        GroupAdapter groupAdapter = new GroupAdapter(groups.getGroups() , getContext());
        recyclerView.setAdapter(groupAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.group_create_button_toolbar:
                NavigationController navigationController = (NavigationController) getActivity();
                navigationController.goToGroupCreate();
        }
    }
}