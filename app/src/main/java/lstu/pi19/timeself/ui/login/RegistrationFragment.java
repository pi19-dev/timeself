package lstu.pi19.timeself.ui.login;

import android.graphics.Color;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import lstu.pi19.timeself.R;
import lstu.pi19.timeself.data.User;
import lstu.pi19.timeself.ui.NavigationController;

public class RegistrationFragment extends Fragment implements View.OnClickListener {

    User user = User.getUser();
    final int MIN_LENGTH = 3;
    final int MAX_LENGTH = 256;
    final String name = "RegFragment";
    TextView firstName, secondName, login, password, confirmPassword, errorView;
    EditText editFirstName, editLastName, editLogin, editPassword, confirmEditPassword;
    Button registrationButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NavigationController navCont = (NavigationController) getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        registrationButton = (Button) view.findViewById(R.id.button_registration);
        registrationButton.setOnClickListener(this::onClick);


        firstName = view.findViewById(R.id.title_first_name);
        errorView = view.findViewById(R.id.errorView);

        editFirstName = (EditText) view.findViewById(R.id.edit_first_name);

        secondName = view.findViewById(R.id.second_name);
        editLastName = (EditText) view.findViewById(R.id.edit_second_name);
        secondName = view.findViewById(R.id.title_second_name);
        editLastName = (EditText) view.findViewById(R.id.edit_second_name);

        login = view.findViewById(R.id.login);
        editLogin = (EditText) view.findViewById(R.id.edit_login);

        password = view.findViewById(R.id.password);
        editPassword = (EditText) view.findViewById(R.id.edit_password);

        confirmPassword = view.findViewById(R.id.confirm_pssword);
        confirmEditPassword = (EditText) view.findViewById(R.id.confirm_edit_password);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_registration:
                buttonRegistrationClick();
                break;
        }
    }

    private void buttonRegistrationClick() {
        clear();
        if (isCorrectAll()) {
            user.setFirstName(editFirstName.getText().toString());
            user.setLastName(editLastName.getText().toString());
            user.setLogin(editLogin.getText().toString());
            user.setPassword(editPassword.getText().toString());
        } else {

        }
    }

    private void clear() {
        errorView.setText("");
        firstName.setTextColor(Color.BLACK);
        secondName.setTextColor(Color.BLACK);
        login.setTextColor(Color.BLACK);
        password.setTextColor(Color.BLACK);
        confirmPassword.setTextColor(Color.BLACK);
    }

    private boolean isCorrectAll() {
        if (isCorrectFirstName() & isCorrectSecondName() & isCorrectLogin() & isCorrectPassword() & isCorrectConfirmPassword()) {
            return true;
        } else return false;
    }

    private boolean isCorrectString(String string) {
        if (string.length() > MIN_LENGTH && string.length() < MAX_LENGTH) {
            return true;
        }
        return false;
    }


    private boolean isCorrectFirstName() {
        String value = editFirstName.getText().toString();
        if (isCorrectString(value)) return true;
        else {
            firstName.setTextColor(getResources().getColor(R.color.errorColor));
            errorView.setText(errorView.getText() + "\nНекорректно введено имя.");
            return false;
        }
    }


    private boolean isCorrectSecondName() {
        String value = editLastName.getText().toString();
        if (isCorrectString(value)) return true;
        else {
            secondName.setTextColor(getResources().getColor(R.color.errorColor));
            errorView.setText(errorView.getText() + "\nНекорректно введена фамилия.");
            return false;
        }
    }


    private boolean isCorrectLogin() {
        String value = editLogin.getText().toString();
        if (isCorrectString(value)) return true;
        else {
            login.setTextColor(getResources().getColor(R.color.errorColor));
            errorView.setText(errorView.getText() + "\nНекорректно введен логин.");
            return false;
        }
    }


    private boolean isCorrectPassword() {
        String value = editPassword.getText().toString();
        if (isCorrectString(value)) return true;
        else {
            password.setTextColor(getResources().getColor(R.color.errorColor));
            errorView.setText(errorView.getText() + "\nНекорректно введен пароль.");
            return false;
        }
    }


    private boolean isCorrectConfirmPassword() {
        String value = confirmEditPassword.getText().toString();
        if (isCorrectString(value)) return true;
        else {
            confirmPassword.setTextColor(getResources().getColor(R.color.errorColor));
            errorView.setText(errorView.getText() + "\nНекорректно введен повторный пароль.");
            return false;
        }
    }

}